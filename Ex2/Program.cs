﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex2
{
    //    2. Ввести вес и рост человека.Рассчитать и вывести индекс массы тела(ИМТ) по формуле
    //I=m/(h* h); где m — масса тела в килограммах, h — рост в метрах.

    class Ex2
    {
        static void Main(string[] args)
        {
            double h, m, i;
            Console.WriteLine("Введите Ваш вес в килограммах:");
            m = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите свой рост в метрах:");
            h = Convert.ToDouble(Console.ReadLine());

            i = m / (h * h);
            Console.WriteLine($"Ваш IMT индекс = {i}");

            Console.ReadKey();
        }
    }
}
