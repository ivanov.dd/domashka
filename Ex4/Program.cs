﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex4
{
    class Program
    {
        //        4. Написать программу обмена значениями двух переменных:
        //а) с использованием третьей переменной;
        static void Main(string[] args)
        {
            int x, y, t;
            Console.WriteLine("Введите переменную x:");
            x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите переменную y:");
            y = Convert.ToInt32(Console.ReadLine());
            t = x;
            x = y;
            y = t;
            Console.WriteLine($"Значения переменных после обмена: x={x}, y={y}");
            Console.ReadKey();

        }
    }
}
